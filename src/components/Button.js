function Button({ text, isStopwatchStart, onStart }) {
  return (
    <button
      className={"btn " + (isStopwatchStart ? "pause" : "start")}
      onClick={() => onStart()}
    >
      {text}
    </button>
  );
}
// Button.defaultProps = {
//   text: "Button",
// };
export default Button;
