import { timeFormat } from "./../helpers/timeFormat";
function StopWatch({ time, onFixTime, isStopwatchStart, isMoved }) {
  const { counter, seconds, minutes } = time;
  return (
    <div
      className={"stopwatch " + (isMoved ? "isMoved" : "")}
      onClick={() => (isStopwatchStart ? onFixTime(time) : null)}
    >
      <span>{minutes}:</span>
      <span>{timeFormat(seconds)}.</span>
      <span>{timeFormat(counter)} </span>
    </div>
  );
}

export default StopWatch;
