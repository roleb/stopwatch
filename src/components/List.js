import { timeFormat } from "./../helpers/timeFormat";

function List({ lists }) {
  return (
    <div className="list-wrap">
      <div className="list">
        {lists.map((item, index) => (
          <div key={index}>
            <span className="list-index">#{timeFormat(index + 1)}</span>
            {item.minutes} - {timeFormat(item.seconds)}.
            {timeFormat(item.counter)}
          </div>
        ))}
      </div>
    </div>
  );
}

export default List;
