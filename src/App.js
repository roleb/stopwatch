import { useState } from "react";
import Button from "././components/Button";
import Stopwatch from "././components/Stopwatch";
import List from "././components/List";

function App() {
  const [stopwatchId, setStopwatchId] = useState(null);
  const [time, setTime] = useState({ counter: 0, seconds: 0, minutes: 0 });
  const [fixedTimes, setFixedTimes] = useState([]);
  const [isStopwatchStart, setIsStopwatchStart] = useState(false);
  const [isMoved, setIsMoved] = useState(false);
  const SPEED_OF_INTERVAL = 10;
  let ms = time.counter,
    s = time.seconds,
    m = time.minutes;

  const startStopwatch = () => {
    if (ms >= 99) {
      s++;
      ms = 0;
    }
    if (s >= 59) {
      m++;
      s = 0;
    }
    ms++;
    setTime({ counter: ms, seconds: s, minutes: m });
  };

  const startClickHandler = () => {
    startStopwatch();
    let _stopwatchId = setInterval(startStopwatch, SPEED_OF_INTERVAL);
    setStopwatchId(_stopwatchId);
    setIsStopwatchStart(true);
  };

  const stopClickHandler = () => {
    clearInterval(stopwatchId);
    setIsStopwatchStart(false);
  };

  const fixTimeHandler = (val) => {
    setFixedTimes((fixedTimes) => [...fixedTimes, val]);
    setIsMoved(true);
  };

  return (
    <>
      <Stopwatch
        time={time}
        onFixTime={fixTimeHandler}
        isStopwatchStart={isStopwatchStart}
        isMoved={isMoved}
      />

      <List lists={fixedTimes} />

      <Button
        onStart={isStopwatchStart ? stopClickHandler : startClickHandler}
        isStopwatchStart={isStopwatchStart}
      />
    </>
  );
}

export default App;
