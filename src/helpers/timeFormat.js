export const timeFormat = (val) => {
  return String(val).padStart(2, "0");
};
